#!/usr/bin/env python3

# Author: Eric Nguyen
# Challenge: Transcribing DNA into RNA

def transcribe_dna(dna_string):
    """
    Take a string of DNA and return its transcribed RNA string.
    """
    return dna_string.replace("T", "U")

if __name__ == "__main__":
    import sys
    try:
        rosalind_txt = open(sys.argv[1], 'r')
        dna_string = rosalind_txt.read()
        print(transcribe_dna(dna_string))
    except IndexError:
        print("You need to specify the filename as an argument.")
    except FileNotFoundError:
        print("File not found.")
